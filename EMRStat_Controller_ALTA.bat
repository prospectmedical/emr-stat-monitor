@ECHO OFF

set echnBatch=EMRStat_ECHN_ALTA
set wtbyBatch=EMRStat_WTBY_ALTA
set eoghBatch=EMRStat_EOGH_ALTA
set covidBatch=EMRStat_COVID19_ALTA
set bat=bat

START /MIN CMD.EXE /C %echnBatch%.%bat%
START /MIN CMD.EXE /C %wtbyBatch%.%bat%
START /MIN CMD.EXE /C %eoghBatch%.%bat%
START /MIN CMD.EXE /C %covidBatch%.%bat%

EXIT