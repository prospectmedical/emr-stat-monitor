@ECHO OFF

set Homefile=\\Homefile02\DataExpress\COVID19\Backup\
set userSystem=C:\Users\%USERNAME%\
set report=wtby_covid19_orders_
set type=txt
set reportName=WTBY
set destination=Homefile02
set fileExist=true
set AppLogicUrl="https://prod-08.westus2.logic.azure.com:443/workflows/034681cecff54fdbb231f799ac02cec1/triggers/manual/paths/invoke?api-version=2016-10-01&sp=/triggers/manual/run&sv=1.0&sig=gXc4kY5Y817UcEND1mWCIxr60jGgzv1lqfc5__VYAEM"

For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set date=%%c%%a%%b)

set reportFile="%report%%date%.%type%"
set FileCheck="%Homefile%%report%%date%*.%type%"

CALL :FileFunc ""
if %fileExist%==false (
TIMEOUT /T 900
CALL :FileFunc "2nd Attempt"
EXIT /B %ERRORLEVEL% 
)

EXIT /B %ERRORLEVEL% 

:FileFunc 
    if exist %FileCheck% (
        set fileExist=true
        FOR %%A in (%FileCheck%) DO @(curl -X POST -H "Content-Type: application/json" --data-raw "{\"Message\": \"%~1 Success %destination% %reportName%: %report% %date-%.%type% at %%~tA\",\"Subject\": \"%~1 Success %reportName% - %destination% - %%~tA\"}" %AppLogicUrl%)
    ) else (
        set fileExist=false
        curl -X POST -H "Content-Type: application/json" --data-raw "{\"Message\": \"%~1 Failed %reportName%: %report% %date-%.%type%\",\"Subject\": \"%~1 Failed %reportName% - %destination%\"}" %AppLogicUrl%
    )
EXIT /B 0

