@ECHO OFF

set ahHomeFileBatch=EMRStat_AH_COVID19_Homefile
set ahAltaBatch=EMRStat_AH_COVID19_ALTA

set bat=bat

START /MIN CMD.EXE /C %ahHomeFileBatch%.%bat%
START /MIN CMD.EXE /C %ahAltaBatch%.%bat%

EXIT