@ECHO OFF

set echnBatch=EMRStat_ECHN_Homefile
set wtbyBatch=EMRStat_WTBY_Homefile
set eoghBatch=EMRStat_EOGH_Homefile
set covidBatch=EMRStat_COVID19_Homefile
set bat=bat

START /MIN CMD.EXE /C %echnBatch%.%bat%
START /MIN CMD.EXE /C %wtbyBatch%.%bat%
START /MIN CMD.EXE /C %eoghBatch%.%bat%
START /MIN CMD.EXE /C %covidBatch%.%bat%

EXIT